# SQL obfuscation

Toy project for a SQL obfuscator lexer, written in Go. It's based on [vitesse.io][1] SQL parser.

## Current results

* The current ``sqlex`` has a right quantization that is easy to extend and to maintain
* It has been tested with the same test cases of a regex quantizer
* For each token it creates a new buffer. This could be improved returing slices ``[]byte``.

### Benchmarks

```
BenchmarkLexer-1   	  200000	      8078 ns/op	    3260 B/op	      49 allocs/op
BenchmarkLexer-2   	  100000	     16486 ns/op	    2392 B/op	      29 allocs/op
BenchmarkRegex-4       20000	     56865 ns/op	    4784 B/op	      37 allocs/op
```

* ``BenchmarkLexer-1`` is a plain tokenizer without the final regex (so it doesn't group)
* ``BenchmarkLexer-2`` is a plain tokenizer with a final regex to remove duplicates

Comparing benchmarks between a lexer and a regex is not a proper compare (apple with orange).
A regex approach **can't** solve this kind of problem, while a lexer is done for that without errors.
Anyway, because performance matter I just provided these benchmarks to have an idea about how much a
lexer is faster than the current regex implementation.

[1]: https://github.com/youtube/vitess
